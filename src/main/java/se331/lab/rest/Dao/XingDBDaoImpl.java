package se331.lab.rest.Dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.XingRepository;

import java.util.List;

@Repository
@Profile("MydbDao")
@Slf4j
public class XingDBDaoImpl implements StudentDao {
    @Autowired
    XingRepository xingRepository;

    @Override
    public List<Student> getAllStudent() {
        log.info("find all student in db");
        return xingRepository.findAll();
    }

    @Override
    public Student findById(Long id) {
        log.info("find student from id {} from database", id);
        return xingRepository.findById(id).orElse(null);
    }

    @Override
    public Student saveStudent(Student student) {
        log.info("save student to database");
        return xingRepository.save(student);
    }

}
