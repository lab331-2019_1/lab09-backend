package se331.lab.rest.Dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Profile("MyDao")
@Repository
public class XingDaoImpl implements StudentDao {
    List<Student> newStudents;

    public XingDaoImpl(){

        this.newStudents = new ArrayList<>();
        this.newStudents.add(Student.builder()
                .id(11l)
                .studentId("602115516")
                .name("Xingchen")
                .surname("Nian")
                .gpa(3.34)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .penAmount(177)
                .description("The great girl ever!!!!")
                .build());
        this.newStudents.add(Student.builder()
                .id(12l)
                .studentId("SE551*")
                .name("Prayuth")
                .surname("The minister")
                .gpa(4.00)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .penAmount(5)
                .description("The great ehhh ever!!!!")
                .build());
        this.newStudents.add(Student.builder()
                .id(13l)
                .studentId("SE551**")
                .name("blahblah")
                .surname("The ??")
                .gpa(2.22)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .penAmount(32)
                .description("The great something ever!!!!")
                .build());

    }

//    @Override
//    public List<Student> getAllStudent() {
//        return newStudents;
//    }

    @Override
    public List<Student> getAllStudent() {
        log.info("My dao is called");
        return this.newStudents;
    }



    @Override
    public Student findById(Long id) {
        return newStudents.get((int) (id -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) newStudents.size());
        newStudents.add(student);
        return student;
    }

}
