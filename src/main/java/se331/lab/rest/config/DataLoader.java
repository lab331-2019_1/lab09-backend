package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se331.lab.rest.repository.XingRepository;

import static se331.lab.rest.entity.Student.builder;

@Component
public class DataLoader implements ApplicationRunner {

        @Autowired
        //StudentRepository studentRepository;
        XingRepository xingRepository;
            @Override
            public void run(ApplicationArguments args) throws Exception {
//                   studentRepository.save(builder()
//                                    .id(1l)
//                                    .studentId("SE-001")
//                                    .name("Prayuth")
//                                    .surname("The minister")
//                                    .gpa(3.59)
//                    .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
//                                    .penAmount(15)
//                                    .description("The great man ever!!!!")
//                                    .build());
//                    studentRepository.save(builder()
//                                    .id(2l)
//                                    .studentId("SE-002")
//                                    .name("Cherprang ")
//                                    .surname("BNK48")
//                                    .gpa(4.01)
//                    .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92")
//                                    .penAmount(2)
//                                    .description("Code for Thailand")
//                                    .build());
//                    studentRepository.save(builder()
//                                    .id(3l)
//                                    .studentId("SE-003")
//                                    .name("Nobi")
//                                    .surname("Nobita")
//                                    .gpa(1.77)
//                    .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
//                                    .penAmount(0)
//                                    .description("Welcome to Olympic")
//                                    .build());

                    xingRepository.save(builder()
                                    .id(11l)
                                    .studentId("xing11")
                                    .name("NNN")
                                    .surname("NNN")
                                    .gpa(2.00)
                                    .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
                                    .penAmount(5)
                                    .description("Welcome to XXX")
                                    .build());

                    xingRepository.save(builder()
                                    .id(22l)
                                    .studentId("xing22")
                                    .name("XXX")
                                    .surname("XXX")
                                    .gpa(3.00)
                                    .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
                                    .penAmount(10)
                                    .description("Welcome to CCC")
                                    .build());

                    xingRepository.save(builder()
                                    .id(33l)
                                    .studentId("xing33")
                                    .name("CCC")
                                    .surname("CCC")
                                    .gpa(4.00)
                                    .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
                                    .penAmount(15)
                                    .description("Welcome to NNN")
                                    .build());


                }


}

